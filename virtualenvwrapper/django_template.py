import logging
import os
import os.path as path
import shutil
from subprocess import check_call
from contextlib import contextmanager

log = logging.getLogger('virtualenvwrapper.django_template')


# yapf: disable
POSTACTIVATE_FMT = '''
export OLD_DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export DJANGO_SETTINGS_MODULE="%s.settings"
'''

POSTDEACTIVATE_FMT = '''
export DJANGO_SETTINGS_MODULE=$OLD_DJANGO_SETTINGS_MODULE
export OLD_DJANGO_SETTINGS_MODULE=""
'''

NEW_SETTINGS = """
INSTALLED_APPS += (
    'compressor', 'django_extensions',
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder'
    'compressor.finders.CompressorFinder',
)

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'
"""
# yapf: enable


def pip_install(*args):
    PIP_PREFIX = ('pip', 'install')
    check_call(PIP_PREFIX + args)


@contextmanager
def temp_cd(temp_path):
    original_path = os.getcwd()
    os.chdir(temp_path)
    yield
    os.chdir(original_path)


def mktemplate(args):
    DIR = path.abspath(path.dirname(__file__))
    project_name, project_path = args
    # upgrade pip
    pip_install('--upgrade', 'pip')
    # install requirements
    requirements = path.join(DIR, 'requirements.txt')
    pip_install('-r', requirements)
    # make template using heroku-django-template
    TEMPLATE_URL = "https://github.com/heroku/heroku-django-template/archive/master.zip"  # noqa
    startproject_args = [
        'django-admin', 'startproject', '--template=%s' % TEMPLATE_URL,
        '--name=Procfile', project_name, project_path
    ]
    check_call(startproject_args)
    # edit template
    with temp_cd(project_path):
        shutil.copy(requirements, 'requirements.txt')
        # write common settings
        with open(path.join(project_name, 'settings.py'), 'a') as settings:
            settings.write(NEW_SETTINGS)
    # write postactivate and postdeactivate hooks for DJANGO_SETTINGS_MODULE
    venv_dir = os.getenv('WORKON_HOME')
    with temp_cd(path.join(venv_dir, project_name, 'bin')):
        with open('postactivate', 'w') as postactivate:
            postactivate.write(POSTACTIVATE_FMT % project_name)
        with open('postdeactivate', 'w') as postdeactivate:
            postdeactivate.write(POSTDEACTIVATE_FMT)


def post_mkproject_source(args):
    project_name = path.basename(os.getenv('VIRTUAL_ENV', ''))
    return POSTACTIVATE_FMT % project_name
