PROJECT = 'virtualenvwrapper.django_template'
VERSION = '0.2'

from setuptools import setup, find_packages

try:
    long_description = open('./README.md', 'rt').read()
except IOError:
    long_description = ''

setup(
    name=PROJECT,
    version=VERSION,
    description="Personal template for Django projects",
    long_description=long_description,
    author="Aniran Chandravongsri",
    author_email="aniran.chandravongsri@gmail.com",
    provides=["virtualenvwrapper.django"],
    requires=["virtualenv", "virtualenvwrapper"],
    namespace_packages=["virtualenvwrapper"],
    packages=find_packages(),
    package_data={
        "virtualenvwrapper": [
            'requirements.txt'
        ]
    },
    entry_points={
        'virtualenvwrapper.project.template': (
            'django=virtualenvwrapper.django_template:mktemplate', ),
        'virtualenvwrapper.project.post_mkproject_source': (
            'django=virtualenvwrapper.django_template:post_mkproject_source')
    }, )
